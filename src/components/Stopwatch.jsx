import React, { Component } from 'react'
import './Stopwatch.css'
export default class Stopwatch extends Component {
    constructor(props) {
        super(props)
        this.state = {
            resume: "",
            reset: ""
        }
    }
    componentWillUnmount() {
        this.props.unmountTime()
    }
    buttonTypeHandler = () => {
        if (this.props.state.timerButton == 'Start') {
            this.props.changeStartToStop()
            this.props.startTimer()
        }
        else if (this.props.state.timerButton == 'Stop') {
            this.props.changeStopToNull()
            this.props.pauseTimer()
            this.setState({
                resume: 'Resume',
                reset: 'Reset'
            })
        }
    }
    resumeClick = () => {
        this.props.startTimer()
        this.setState({
            resume: '',
            reset: ''
        })
        this.props.changeStartToStop()
    }
    resetClick = () => {
        this.setState({
            resume: '',
            reset: ''
        })
        this.props.unmountTime()
    }
    timeFormatter = () => {
        let hour = this.props.state.hour + ''
        if (hour.length == 1) {
            hour = '0' + hour
        }
        let minute = this.props.state.minute + ''
        if (minute.length == 1) {
            minute = '0' + minute
        }
        let seconds = this.props.state.seconds + ''
        if (seconds.length == 1) {
            seconds = '0' + seconds
        }
        let mili = this.props.state.mili + ''
        if (mili.length == 1) {
            mili = '0' + mili
        } else if (mili.length == 3) {
            mili = "00"
        }
        if (hour != 0 || minute != 0 || seconds != 0 || mili != 0) {
            return <p className='text-6xl'>{hour}:{minute}:{seconds}:{mili}</p>
        }
    }

    render() {
        return (
            <div className='text-white bg-gray-900 h-72 w-96 rounded-md border-2 border-slate-400 relative p-5 stopwatch'>
                <button onClick={this.props.closeTimer} className='font-black text-2xl absolute close-button'>X</button>
                <h2 className='text-white text-4xl '>Stopwatch</h2>
                {this.timeFormatter()}
                <div>
                    {
                        this.props.state.timerButton ?
                            <button onClick={this.buttonTypeHandler} className='text-white bg-gray-800 px-5 py-3 rounded-lg hover:bg-blue-500'>
                                {this.props.state.timerButton}
                            </button>
                            :
                            null
                    }

                    {
                        this.state.resume ?
                            <button onClick={this.resumeClick} className='text-white mr-4 bg-gray-800 px-5 py-3 rounded-lg hover:bg-blue-500'>
                                {this.state.resume}
                            </button>
                            :
                            null
                    }
                    {
                        this.state.reset ?
                            <button onClick={this.resetClick} className='text-white bg-gray-800 px-5 py-3 rounded-lg hover:bg-blue-500'>
                                {this.state.reset}
                            </button>
                            :
                            null
                    }

                </div>
            </div>

        )
    }
}
