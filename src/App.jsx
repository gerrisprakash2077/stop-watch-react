import React, { Component } from 'react'
import './App.css'
import Stopwatch from './components/Stopwatch'
export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      displayTimer: false,
      timerButton: 'Start',
      hour: 0,
      minute: 0,
      seconds: 0,
      mili: 0
    }
    this.interval = null
  }
  displayTimer = () => {
    this.setState({
      displayTimer: true
    })
  }
  closeTimer = () => {
    this.setState({
      displayTimer: false
    })
  }
  changeStartToStop = () => {
    this.setState({
      timerButton: 'Stop'
    })
  }
  changeStopToNull = () => {
    this.setState({
      timerButton: ''
    })
  }
  startTimer = () => {
    this.interval = setInterval(() => {
      if (this.state.minute === 60) {
        this.setState({
          hour: this.state.hour + 1,
          minute: 0,
          seconds: 0,
          mili: 0
        })
      }
      else if (this.state.seconds === 60) {
        this.setState({
          minute: this.state.minute + 1,
          seconds: 0,
          mili: 0
        })
      }
      else if (this.state.mili === 100) {
        this.setState({
          seconds: this.state.seconds + 1,
          mili: 0
        })
      } else {
        this.setState({
          mili: this.state.mili + 1
        })
      }
    }, 10)
  }
  pauseTimer = () => {
    clearInterval(this.interval)
  }
  unmountTime = () => {
    clearInterval(this.interval)
    this.setState({
      hour: 0,
      minute: 0,
      seconds: 0,
      mili: 0,
      timerButton: 'Start'
    })
  }
  render() {
    return (
      <>
        <header >
          <h1 className='text-white text-5xl text-center p-10'>🚀Timers🚀</h1>
        </header>
        <section className='flex justify-center'>
          {this.state.displayTimer ? <Stopwatch pauseTimer={this.pauseTimer} changeStopToNull={this.changeStopToNull} changeStartToStop={this.changeStartToStop} unmountTime={this.unmountTime} startTimer={this.startTimer} closeTimer={this.closeTimer} state={this.state} /> :
            <button onClick={this.displayTimer} className='text-white bg-gray-800 px-5 py-3 rounded-lg hover:bg-blue-500'>
              Show Stopwatch
            </button>}
        </section>
      </>
    )
  }
}
